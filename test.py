import requests

import pytest


class TestMethodsClass(object):
    def test_method_get(self):
        response = requests.get(url='https://httpbin.org/get')
        assert response.status_code == 200

    def test_method_post(self):
        response = requests.post(url='https://httpbin.org/post')
        assert response.status_code == 201

    def test_method_delete(self):
        response = requests.delete(url='https://httpbin.org/delete')
        assert response.status_code == 200